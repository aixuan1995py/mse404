Lab 8: Spin Polarization and Magnetic Systems
=============================================

[Back to Course Overview](..)

Up till now we have been assuming that we always had some set of bands which
could each fit two identical electrons. Essentially we have been ignoring
the electron spin. If you want to examine, for example, a magnetic system
then the spin of the electrons is important. It can also be important
in modelling atomic or molecular systems. We'll cover several examples
of this in this lab.

The Hydrogen Atom and electron spin
-----------------------------------

As a first example, let's look again at hydrogen. In the molecule we have
two hydrogen atoms bonded together, and two electrons in total, and
we can do this with a standard calculation.

But if we instead want to find the energy of an isolated H atom accurately
it's a little more tricky. A hydrogen atom has one electron, which means if we
treat it with doubly-degenerate bands, we would have a single half occupied
band. To calculate it like this we can treat it as a metal, and use some small
smearing, so that we allow partial occupation of bands. This is equivalent to
assuming that we have half an electron in each spin state. If we however
restrict it to being in one spin state or another, we may find a slightly
different energy (coming primarily from differences in how the DFT exchange
term is calculated in each case, with the latter being more physical).

To perform a spin-polarized calculation in quantum espresso with the `pw.x`
code, there are two additional variables you'll need to set.

- `nspin`: this is 1 by default so no spin polarization is taken into account.
  To perform a spin polarized calculation it should be set to 2.
- `tot_magnetization`: this is difference between the number of spin-up and
  spin-down electrons in the cell. If we want a single spin up electron
  we can set this to `1.0`.

The directory [`01_H1_metal`](01_H1_metal) has an input file for a single
H atom using a small smearing, while the directory [`01_H1_spin`](01_H1_spin)
has the same calculation, but with no smearing, and we have used the two
input variables above to enable a spin polarized calculation.

- Run the input files in these two folders.
- Compare the total energy obtained in each case. In which case is the
  energy lower?
- Compare the energies of the lowest energy calculated bands.
    - Enabling smearing for the "metal" calculation will automatically add
      extra bands, but only the lowest energy band will be occupied in this
      calculation.
    - In the spin polarized calculation if you check the output you will see
      two sections for the band energies. One listing the energies of the
      "spin up" bands, and the other listing "spin down" bands. Since we
      have said we want 1 spin up electron in the calculation, the spin up
      band will be occupied and should be lower in energy than the
      unoccupied spin down band.
- Slightly above the band energies in the output of the spin polarized
  calculation, you'll see that quantum espresso also outputs the magnetic
  moment associated with each atom in the system. And slightly below the final
  total energy output, it will list the total magnetization of the system in
  Bohr magnetons per cell. The  measured value for hydrogen is 1. How close
  are you here?

The Oxygen Molecule
-------------------

Even if a system is not necessarily magnetic and we might imagine that
representing it with some set of fully occupied, doubly degenerate bands will
work, including spin polarization can lead to important differences. One
example of this is the O2 molecule.

In this case, we have a system with two interacting oxygen atoms. Each oxygen
has 8 electrons in total, with the configuration 1s2 2s2 2p4 (the 1s orbital
will be contained within the pseudopotential for the DFT calculations done
here, so you will have 6 electrons from each oxygen atom). For a single
oxygen, from Hund's rule the three p orbitals should be filled singly before
being filled in pairs, so that one of the p-orbitals will have two electrons,
and the other two should have one each. However, if we assume doubly occupied
orbitals, we'll have the two p-orbitals with two electrons and one that is
empty. This means a calculation where we assume a set of doubly occupied bands
will have trouble converging to the ground state of the system. For the
molecule the situation is similar, but the s and p orbitals from each atom
combine to form bonding and anti-bonding sigma and pi orbitals.

The directory [`02_O2`](02_O2) contains an input file to calculate the total
energy of the system at the measured bond length. Here the calculation has
been set up exactly as you've seen in the past.

- Try running this calculation and see what happens.

While it's possible that the system may randomly meet the convergence
criteria in the self-consistent cycle, this calculation will most likely
not converge. If you look at the estimate accuracy at the end of each
iteration in the output, it will likely vary from step to step, rather than
steadily decreasing as in a well-behaved calculation.

The situation we have is similar to a metal: we have two bands and the ground
state of the system should be when there is one electron in each of them.

- Create a copy of the `02_O2` directory called `02_O2_metal`. Modify the
  input file in it to use a metallic occupation scheme with a small smearing
  width and run the calculation. The relevant input variables have been used
  in the H atom example discussed above.
    - Does the calculation now converge?
    - Take a look at the file `pwscf.xml` in the calculation directory, and
      try to find the occupations of each band at each k-point. Are these as
      expected?

As with the hydrogen case above however, treating this system as a metal may
not get the physics of the system right.

- Create another copy of `02_O2` called `02_O2_spin`. This time modify the
  input file in it to turn on spin polarization, as was done in the hydrogen
  example. This time try setting the total magnetization to 0, which would be
  the case if we don't have any net magnetization in the molecule, as both
  spins point in opposite directions. Then try setting the total magnetization
  to 2.0, which corresponds to both spins pointing in the same direction.
    - How does the total energy compare for each case compare to the metallic
      scheme? Which is the more energetically favourable configuration? How do
      the orbital energies vary?

Magnetic Systems - Iron
-----------------------

Now that you've seen how including spin polarization can allow us a correctly
describe the ground state of our system in your calculation, the next step
is to use it to describe a magnetic system.

In a magnetic system there is a net spin polarization in the unit cell. This
means that we'll probably have an odd number of electrons, and the energy of
the system when we include a net spin polarization is lower than the energy
when we don't.

One of the most common magnetic systems is iron, so we'll examine this.
The directory [`03_Fe`](03_Fe) contains an input file for iron. Note this is
a BCC structure (as set by `ibrav = 3` in the input file), whereas most of the
crystals structures you have examined previously were FCC. The calculation
has been set up in the usual way for a metallic system.

- Run this calculation and check everything worked as expected.
- Now make a copy of the calculation directory and in this, modify the
  calculation to turn on spin polarization. Try running the calculation
  with `tot_magnetization = 0.0` first, and compare your total energy to that
  obtained using doubly degenerate bands. **Note** while in the case of the
  atoms and molecules above, we were able to get our calculations to at
  least converge my using a metallic occupation instead of using spin
  polarization, in the case of iron, it will still be a metal when you use
  spin polarization, so you should not remove the input variables associated
  with this.
    - The total energies should agree within the accuracy of the calculation.
- Now try setting the total magnetization to 1.0 and see how total energy
  changes.
    - Which is the more energetically favourable configuration?
- Try setting the total magnetization to 2.0.
    - How does this compare to the previous value?

From this we could test many guesses for the total magnetization, and find
the value which gives the lowest overall total energy. However, we can instead
pass an option that tells quantum espresso to automatically find the best
value. This is done by setting the `starting_magnetization` input variable.

- Make another copy of the `03_Fe` directory, and this time set `nspin = 2`,
  and `starting_magnetizaton = 1.0` (do not include the `tot_magnetization`
  variable as this fixes a value). Run the calculation and see what the final
  total magnetization per cell is. See if you can find a measured value for
  iron to compare to.

- See if you can use what we covered in previous labs to calculate and make a
  plot of the electronic band structure of BCC Fe.
    - Plot the spin-up and spin-down bands in different colours.
    - There are different Fermi energies for the spin-up and spin-down bands:
      indicate these on your plot in some sensible way.
    - As the Brillouin zone is different to the one's you have calculated so
      far you'll need to select a few sensible high-symmetry points yourself
      to plot with.

------------------------------------------------------------------------------

Summary
-------

In this lab you have seen:

- how to do a DFT calculation including spin polarization.
- how some systems need to be done with spin polarization to converge to
  the correct ground state.
- how to use spin polarized calculations to find the correct magnetization of
  a magnetic system by letting the code find the total magnetization that
  produces the lowest overall total energy.

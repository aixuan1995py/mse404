#!/bin/bash

template="Si_base.in"
repstr="xxxx"

# The "for" construction we've used previously only handles integers.
# So we set an initial value for A and its delta value as variables.
a1=5.30
da=0.02

for i in {00..10..1}
do
  inp="Si_${i}.in"
  # bc is a calculator that can calculate expressions in the shell. We
  # pass it our calculation with echo and save the result as a variable.
  a=$(echo "$a1 + $i * $da" | bc)
  sed "s/$repstr/$a/" $template > $inp
  pw.x < $inp &> ${inp%.*}.out
done

awk '/unit-cell volume/{vol=$4}
     /^!.*total/{print vol, $5}' *out > etot_v_vol.dat


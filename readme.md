Modelling Materials with Density Functional Theory
==================================================

This is the lab material for the Imperial College London, Department of
Materials course "MSE404: Modelling Materials with Density Functional
Theory".

The latest version of the course is available online at
<https://gitlab.com/mse404/mse404>. The text source files named ``readme.md``
in the various directories use markdown. You can download the whole repository
and view them in a terminal, or read them at the gitlab site where they're
automatically rendered a little more nicely. Alternatively this content is
also formatted as a website at <https://mse404.gitlab.io/labs>.

This course is intended to introduce students to the modelling of materials
with density-functional theory (DFT). In the labs we will use the free,
open-source DFT code [Quantum Espresso](http://www.quantum-espresso.org/), but
while the format of the input files may change in other DFT codes, the general
principles will be the same. The labs will also briefly introduce student to
the use of the Linux OS and how it enables the effective use of computational
resources, as well as showing students some simple scripting, analysis, and
visualization tools.

The labs are set up assuming students are using our student server remotely
which has all the necessary software installed already. If it is not possible
for you to do this, I suggest installing xubuntu to a virtual machine on your
laptop. If you need to do this, there are some guidelines at
[vmsetup](vmsetup). Note that the remote desktop software we use,
[x2go](https://wiki.x2go.org) is freely available for Windows, Mac, and Linux
so if you'd prefer to use your own laptop, please go ahead.

The labs are organised as follows:

- [Lab 1: Getting started in Linux](lab01)
- [Lab 2: Quantum Espresso Input and Output](lab02)
- [Lab 3: Converging your Calculations](lab03)
- [Lab 4: The Electronic Band Structure and Density of States](lab04)
- [Lab 5: Forces, Stresses and Structures](lab05)
- [Lab 6: Vibrational Frequencies and Normal Modes](lab06)
- [Lab 7: Finite Temperature Properties](lab07)
- [Lab 8: Spin Polarization and Magnetic Systems](lab08)
- [Lab 9: Optical Properties and Time-Dependent Density Functional Theory](lab09)

Additional Material:

- [Calculating Useful Properties from Total Energies](using_total_energies)
- [Examining the Charge Density and Wavefunction](visualising_output)

Extras:

- [Setting up a virtual machine](vmsetup)
- [More Useful Linux Commands](linuxcommands)
- [Plotting with Gnuplot](gnuplot)
- [Bash and Shell Scripting](shellscripting)
- [Running in Parallel](running_in_parallel)
